using System.Web.Http;
using EuroMoney.Controllers.Person.ViewModels;
using EuroMoney.Controllers.Person.ViewModels.Mapper.Contract;
using EuroMoney.Infrastructure.Repository.PersonRepository.Contracts;

namespace EuroMoney.Controllers.Person 
{
    public class PersonController : ApiController
    {
        private readonly IPersonRepository personRepository;

        private readonly IPersonPageViewModelMapper personPageViewModelMapper;

        private readonly IPersonModelMapper personModelMapper;

        public PersonController(IPersonRepository personRepository, IPersonPageViewModelMapper personPageViewModelMapper, IPersonModelMapper personModelMapper)
        {
            this.personRepository = personRepository;
            this.personPageViewModelMapper = personPageViewModelMapper;
            this.personModelMapper = personModelMapper;
        }

        public PersonPageViewModel Get(int id)
	{
	    var person = this.personRepository.Get(id);
            return this.personPageViewModelMapper.MapFrom(person);
	}

	public void Post([FromBody]PersonFormViewModel formViewModel)
	{
	    var person = this.personModelMapper.MapFrom(formViewModel);
	    this.personRepository.Save(person);
	}
    }
}
