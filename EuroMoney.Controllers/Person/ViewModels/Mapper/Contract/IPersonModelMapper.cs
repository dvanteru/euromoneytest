namespace EuroMoney.Controllers.Person.ViewModels.Mapper.Contract
{
    public interface IPersonModelMapper
    {
        Domain.Person MapFrom(PersonFormViewModel formViewModel);
    }
}