﻿namespace EuroMoney.Controllers.Person.ViewModels.Mapper.Contract 
{
    public interface IPersonPageViewModelMapper
    {
        PersonPageViewModel MapFrom(Domain.Person person);
    }
}
