using System;
using EuroMoney.Controllers.Person.ViewModels.Mapper.Contract;
using EuroMoney.Domain;

namespace EuroMoney.Controllers.Person.ViewModels.Mapper
{
    public class PersonModelMapper : IPersonModelMapper
    {
        public Domain.Person MapFrom(PersonFormViewModel formViewModel)
        {
            return new Domain.Person
                {
                    Id = int.Parse(formViewModel.Id),
                    BirthDetails = BirthDetailsMapper(formViewModel),
                    Personality = PersonalityMapper(formViewModel),
                    SocialMedia = SocialMediaMapper(formViewModel),
                    FamilyName = formViewModel.FamilyName,
                    GivenName = formViewModel.GivenName,
                    MiddleName = formViewModel.MiddleName
                };
        }

        private static SocialMedia SocialMediaMapper(PersonFormViewModel formViewModel)
        {
            return new SocialMedia {TwitterId = formViewModel.TwitterId};
        }

        private static Personality PersonalityMapper(PersonFormViewModel formViewModel)
        {
            return new Personality
                {
                    Height =
                        string.IsNullOrEmpty(formViewModel.Height) ? 0 : Convert.ToDecimal(formViewModel.Height)
                };
        }

        private static BirthDetails BirthDetailsMapper(PersonFormViewModel formViewModel)
        {
            return new BirthDetails
                {
                    DateOfBirth =
                        string.IsNullOrEmpty(formViewModel.DateOfBirth)
                            ? (DateTime?) null
                            : Convert.ToDateTime(formViewModel.DateOfBirth),
                    DateOfDeath =
                        string.IsNullOrEmpty(formViewModel.DateOfDeath)
                            ? (DateTime?) null
                            : Convert.ToDateTime(formViewModel.DateOfDeath),
                    PlaceOfBirth = formViewModel.PlaceOfBirth
                };
        }
    }
}