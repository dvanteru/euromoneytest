﻿using System;
using EuroMoney.Controllers.Person.ViewModels.Mapper.Contract;

namespace EuroMoney.Controllers.Person.ViewModels.Mapper
{
    public class PersonPageViewModelMapper : IPersonPageViewModelMapper
    {
        public PersonPageViewModel MapFrom(Domain.Person person)
        {
            var pageViewModel = new PersonPageViewModel
                {
                    Id = person.Id,
                    Age =
                        person.BirthDetails.DateOfDeath.HasValue
                            ? person.BirthDetails.DateOfDeath.Value - person.BirthDetails.DateOfBirth.Value
                            : DateTime.Now - person.BirthDetails.DateOfBirth.Value,
                    DateOfBirth = person.BirthDetails.DateOfBirth.Value,
                    DateOfDeath = person.BirthDetails.DateOfDeath.HasValue ? person.BirthDetails.DateOfDeath.Value : (DateTime?) null,
                    FamilyName = person.FamilyName,
                    GivenName = person.GivenName,
                    Height = person.Personality.Height,
                    MiddleName = person.MiddleName,
                    PlaceOfBirth = person.BirthDetails.PlaceOfBirth,
                    TwitterId = person.SocialMedia.TwitterId
                };
            return pageViewModel;
        }
    }
}