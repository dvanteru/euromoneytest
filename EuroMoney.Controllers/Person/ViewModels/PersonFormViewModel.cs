using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace EuroMoney.Controllers.Person.ViewModels
{
    [DataContract]
    [XmlRoot(ElementName = "Person")]
    public class PersonFormViewModel
    {
	[DataMember]
        public string Id { get; set; }

	[DataMember]
        public string FamilyName { get; set; }

	[DataMember]
        public string GivenName { get; set; }

	[DataMember]
        public string MiddleName { get; set; }

	[DataMember]
        public string DateOfBirth { get; set; }

	[DataMember]
        public string DateOfDeath { get; set; }

	[DataMember]
        public string PlaceOfBirth { get; set; }

	[DataMember]
        public string Height { get; set; }

	[DataMember]
        public string TwitterId { get; set; }
    }
}