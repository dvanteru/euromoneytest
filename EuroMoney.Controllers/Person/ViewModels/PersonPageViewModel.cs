﻿using System;

namespace EuroMoney.Controllers.Person.ViewModels 
{
    public class PersonPageViewModel 
    {
        public int Id { get; set; }
        public string FamilyName { get; set; }
        public string GivenName { get; set; }
        public string MiddleName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime? DateOfDeath { get; set; }
        public string PlaceOfBirth { get; set; }
        public decimal Height { get; set; }
        public string TwitterId { get; set; }
        public TimeSpan Age { get; set; } 
    }
}
