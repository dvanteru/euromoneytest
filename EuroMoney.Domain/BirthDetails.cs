﻿using System;

namespace EuroMoney.Domain
{
    public class BirthDetails
    {
        public DateTime? DateOfBirth { get; set; }
        public DateTime? DateOfDeath { get; set; }
        public string PlaceOfBirth { get; set; } 
    }
}