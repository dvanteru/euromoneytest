﻿using EuroMoney.Framework.Shared;

namespace EuroMoney.Domain
{
    public class Person : Entity
    {      
        public string FamilyName { get; set; }
        public string GivenName { get; set; }
        public string MiddleName { get; set; }
        public BirthDetails BirthDetails { get; set; }
        public Personality Personality { get; set; }
        public SocialMedia SocialMedia { get; set; }
    }
}