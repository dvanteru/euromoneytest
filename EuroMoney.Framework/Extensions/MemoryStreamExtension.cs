﻿using System.IO;

namespace EuroMoney.Framework.Extensions 
{
    public static class MemoryStreamExtension 
    {
        public static MemoryStream CreateMemoryStream(this FileStream fileStream, ref MemoryStream memoryStream) 
        {
            var bytes = new byte[fileStream.Length];
            fileStream.Read(bytes, 0, (int)fileStream.Length);
            memoryStream.Write(bytes, 0, (int)fileStream.Length);
            return memoryStream;
        }
    }
}
