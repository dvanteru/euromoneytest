using System.Collections.Generic;
using System.IO;

namespace EuroMoney.Framework.Extensions
{
    public static class TextFileExtensions
    {
        public static IDictionary<string, string> TextParser(this MemoryStream memoryStream)
        {
	    IDictionary<string, string> personDictionary = new Dictionary<string, string>(); 

            memoryStream.Seek(0, SeekOrigin.Begin);
            using (var streamReader = new StreamReader(memoryStream)) {
                while (streamReader.Peek() >= 0)
                {
                    var line = streamReader.ReadLine();
                    if (line != null) personDictionary.Add(line.Split('=')[0], line.Split('=')[1]);
                }
            }
            return personDictionary; 
        }
    }
}