﻿using EuroMoney.Framework.Shared;

namespace EuroMoney.Framework.Repository 
{
    public interface IRepository<T> where T : Entity
    {
        T Get(int id);
        void Save(T entity);
    }
}
