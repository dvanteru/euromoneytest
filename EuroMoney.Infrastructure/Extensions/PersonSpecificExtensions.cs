﻿using System;
using System.Collections.Generic;
using System.Reflection;
using EuroMoney.Domain;

namespace EuroMoney.Infrastructure.Extensions 
{
    public static class PersonSpecificExtensions 
    {
        public static Person MapToPerson(this IDictionary<string, string> personDictionary)
        {
            string id, familyName, givenName, middleNames, dateOfBirth, dateOfDeath, placeOfBirth, height, twitterId;

            personDictionary.TryGetValue("id", out id);
            personDictionary.TryGetValue("familyName", out familyName);
            personDictionary.TryGetValue("givenName", out givenName);
            personDictionary.TryGetValue("middleNames", out middleNames);
            personDictionary.TryGetValue("dateOfBirth", out dateOfBirth);
            personDictionary.TryGetValue("dateOfDeath", out dateOfDeath);
            personDictionary.TryGetValue("placeOfBirth", out placeOfBirth);
            personDictionary.TryGetValue("height", out height);
            personDictionary.TryGetValue("twitterId", out twitterId);

            var person = PersonFromDictionary(id, dateOfBirth, dateOfDeath, placeOfBirth, familyName, givenName, middleNames, height, twitterId);
            return person;
        }

        private static Person PersonFromDictionary(string id, string dateOfBirth, string dateOfDeath, string placeOfBirth,
                                                   string familyName, string givenName, string middleNames, string height,
                                                   string twitterId)
        {
            var person = new Person
                {
                    Id = Int32.Parse(id),
                    BirthDetails = new BirthDetails
                        {
                            DateOfBirth = Convert.ToDateTime(dateOfBirth),
                            DateOfDeath = String.IsNullOrEmpty(dateOfDeath) ? (DateTime?) null : Convert.ToDateTime(dateOfDeath),
                            PlaceOfBirth = placeOfBirth
                        },
                    FamilyName = familyName,
                    GivenName = givenName,
                    MiddleName = middleNames,
                    Personality = new Personality
                        {
                            Height = Convert.ToDecimal(String.IsNullOrEmpty(height) ? null : height)
                        },
                    SocialMedia = new SocialMedia
                        {
                            TwitterId = twitterId
                        }
                };
            return person;
        }

        public static IDictionary<string, string> GetDictionaryValues(this Person person) {
            return new Dictionary<string, string>
                {
                    {"id", person.Id.ToString()},
                    {"familyName", person.FamilyName },
                    {"givenName", person.GivenName},
                    {"middleNames", person.MiddleName},
                    {"dateOfBirth", person.BirthDetails.DateOfBirth.HasValue ? person.BirthDetails.DateOfBirth.Value.ToString() : String.Empty},
                    {"dateOfDeath", person.BirthDetails.DateOfDeath.HasValue ? person.BirthDetails.DateOfDeath.Value.ToString() : String.Empty},
                    {"placeOfBirth", person.BirthDetails.PlaceOfBirth},
                    {"height", person.Personality.Height.ToString()},
                    {"twitterid", person.SocialMedia.TwitterId}
                };
        }
    }
}
