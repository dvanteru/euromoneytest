﻿using System.Collections.Generic;
using System.IO;

namespace EuroMoney.Infrastructure.FileGateway.Contract
{
    public interface IFileGateway
    {
        MemoryStream GetContentsFromFile(string fileName);

        void Save(IDictionary<string, string> personDictionary);
    }
}