﻿using System;
using System.Collections.Generic;
using System.IO;
using EuroMoney.Framework.Extensions;
using EuroMoney.Infrastructure.FileGateway.Contract;

namespace EuroMoney.Infrastructure.FileGateway
{
    public class FileGateway : IFileGateway, IDisposable
    {
        private MemoryStream memoryStream;

        public MemoryStream GetContentsFromFile(string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read)) {
                memoryStream = new MemoryStream();
                memoryStream = fileStream.CreateMemoryStream(ref memoryStream);
            }
            return memoryStream;
        }

        public void Save(IDictionary<string, string> personDictionary)
        {
            string id;
            var basePath = AppDomain.CurrentDomain.BaseDirectory;
            personDictionary.TryGetValue("id", out id);
            var filePath = string.Format("{0}//person-{1}.txt", basePath, id);
            var file = File.Create(filePath);
            using (var writer = new StreamWriter(file))
            {
                foreach (var pair in personDictionary)
                {
                    writer.Write(string.Format("{0}={1}{2}", pair.Key.Trim(), pair.Value.Trim(), System.Environment.NewLine));
                }
            }
        }

        protected virtual void Dispose(bool disposing) {
            if (disposing) {
                // dispose managed resources
                memoryStream.Close();
            }
            // free native resources
        }


        public void Dispose()
        {
           Dispose(true);
	    GC.SuppressFinalize(this);
        }
    }
}