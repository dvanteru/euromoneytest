﻿using EuroMoney.Domain;

namespace EuroMoney.Infrastructure.PersonGateway.Contracts
{
    public interface IPersonGateway
    {
        Person Serialize(int id);

        void Save(Person person);
    }
}