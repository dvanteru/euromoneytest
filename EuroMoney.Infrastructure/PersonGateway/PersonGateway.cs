﻿using System;
using EuroMoney.Domain;
using EuroMoney.Framework.Extensions;
using EuroMoney.Infrastructure.Extensions;
using EuroMoney.Infrastructure.FileGateway.Contract;
using EuroMoney.Infrastructure.PersonGateway.Contracts;

namespace EuroMoney.Infrastructure.PersonGateway
{
    public class PersonGateway : IPersonGateway
    {
        private readonly IFileGateway fileGateway;

        public PersonGateway(IFileGateway fileGateway)
        {
            this.fileGateway = fileGateway;
        }

        public Person Serialize(int id)
        {
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var fileName = string.Format("{0}\\person-{1}.txt", baseDirectory, id.ToString());
            var memoryStream = this.fileGateway.GetContentsFromFile(fileName);
            var personDictionary = memoryStream.TextParser();
            return personDictionary.MapToPerson();
        }

        public void Save(Person person)
        {
            var personDictionary = person.GetDictionaryValues();
	    this.fileGateway.Save(personDictionary);
        }
    }
}