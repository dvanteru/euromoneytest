﻿using EuroMoney.Domain;
using EuroMoney.Framework.Repository;

namespace EuroMoney.Infrastructure.Repository.PersonRepository.Contracts
{
    public interface IPersonRepository : IRepository<Person>
    {
        Person Get(int id);
        void Save(Person person);
    }
}