﻿using System.Collections.Generic;
using EuroMoney.Domain;
using EuroMoney.Infrastructure.Extensions;
using EuroMoney.Infrastructure.FileGateway.Contract;
using EuroMoney.Infrastructure.PersonGateway.Contracts;
using EuroMoney.Infrastructure.Repository.PersonRepository.Contracts;

namespace EuroMoney.Infrastructure.Repository.PersonRepository
{
    public class PersonRepository : IPersonRepository
    {
        private readonly IPersonGateway personGateway;

        public PersonRepository(IPersonGateway personGateway, IFileGateway fileGateway)
        {
            this.personGateway = personGateway;
        }

        public Person Get(int id)
        {
           return personGateway.Serialize(id);
        }

        public void Save(Person person)
        {
            personGateway.Save(person);
        }
    }
}

