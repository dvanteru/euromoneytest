﻿using EuroMoney.Controllers.Person.ViewModels;
using EuroMoney.Controllers.Person.ViewModels.Mapper;
using EuroMoney.Controllers.Person.ViewModels.Mapper.Contract;
using EuroMoney.Domain;
using Machine.Specifications;
using developwithpassion.specifications.rhinomocks;

namespace EuroMoney.Tests.Controllers.Mappers 
{
    public abstract class person_model_mapper_concern : Observes<IPersonModelMapper, PersonModelMapper>
    {
    }

    public class when_asked_to_map_from_person_form_view_model : person_model_mapper_concern
    {
        private Establish c = () =>
            {
                formViewModel = new PersonFormViewModel{ Id = "5"};
            };
        private Because b = () => result = sut.MapFrom(formViewModel);
        private It should_return_person_model = () => result.ShouldNotBeNull();
        private It should_return_person_model_with_id_5 = () => result.Id.ShouldEqual(5);
        private static PersonFormViewModel formViewModel;
        private static Person result;
    }
}
