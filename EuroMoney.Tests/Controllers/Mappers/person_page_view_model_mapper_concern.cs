﻿using System;
using EuroMoney.Controllers.Person.ViewModels;
using EuroMoney.Controllers.Person.ViewModels.Mapper;
using EuroMoney.Controllers.Person.ViewModels.Mapper.Contract;
using EuroMoney.Domain;
using Machine.Specifications;
using developwithpassion.specifications.rhinomocks;

namespace EuroMoney.Tests.Controllers.Mappers 
{
    public abstract class person_page_view_model_mapper_concern : Observes<IPersonPageViewModelMapper, PersonPageViewModelMapper>
    {
    }

    public class when_asked_to_map_from_person_model : person_page_view_model_mapper_concern
    {
        private Establish c = () =>
            {
                person = new Person{ Id = 3, BirthDetails = new BirthDetails{ DateOfBirth = DateTime.Now, DateOfDeath = DateTime.Now}, Personality = new Personality(), SocialMedia = new SocialMedia()};
            };

        private Because b = () => result = sut.MapFrom(person);

        private It should_return_person_pageviewmodel = () => result.ShouldNotBeNull();

        private It should_return_person_pageviewmodel_with_id = () => result.Id.ShouldEqual(3);

        private static Person person;
        private static PersonPageViewModel result;
    }
}
