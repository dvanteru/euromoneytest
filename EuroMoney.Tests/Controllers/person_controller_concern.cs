﻿using EuroMoney.Controllers;
using EuroMoney.Controllers.Person;
using EuroMoney.Controllers.Person.ViewModels;
using EuroMoney.Controllers.Person.ViewModels.Mapper.Contract;
using EuroMoney.Domain;
using EuroMoney.Infrastructure.Repository.PersonRepository.Contracts;
using Machine.Specifications;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace EuroMoney.Tests.Controllers 
{
    public abstract class person_controller_concern : Observes<PersonController>
    {
        private Establish c = () =>
            {
                personRepository = depends.on<IPersonRepository>();
            };

        protected static Person person;
        protected static IPersonRepository personRepository;
    }

    public class when_asked_to_get_person_by_id : person_controller_concern
    {
        private Establish context = () =>
            {
		person = new Person { Id = 3};
                result = new PersonPageViewModel {Id = 3};              
                personRepository.setup(x => x.Get(personId)).Return(person);
                personPageViewModelMapper = depends.@on<IPersonPageViewModelMapper>();
		personPageViewModelMapper.setup(x => x.MapFrom(person)).Return(result);
            };

        private Because b = () => sut.Get(personId);

        private It should_ask_person_repository = () => personRepository.received(x => x.Get(personId));

        private It should_ask_person_pageviewmodel_mapper_to_map_from_person =
            () => personPageViewModelMapper.received(x => x.MapFrom(person));

        private It should_return_person_pageviewmodel = () => result.ShouldNotBeNull();

        private It should_return_person_page_view_model_with_id = () => result.Id.ShouldEqual(3);

        private static int personId;
        private static IPersonPageViewModelMapper personPageViewModelMapper;
        private static PersonPageViewModel result;
    }

    public class when_asked_to_save_person : person_controller_concern
    {
        private Establish c = () =>
            {
                formViewModel = new PersonFormViewModel();
                personModelMapper = depends.on<IPersonModelMapper>();
                personModelMapper.setup(x => x.MapFrom(formViewModel)).Return(person);
                personRepository.setup(x => x.Save(person));
            };

        private Because b = () => sut.Post(formViewModel);

        private It should_ask_personmodelmapper_to_map_to_person = () => personModelMapper.received(x => x.MapFrom(formViewModel));

        private It should_ask_person_repository_to_save = () => personRepository.received(x => x.Save(person));

        private static PersonFormViewModel formViewModel;
        private static IPersonModelMapper personModelMapper;
    }
}
