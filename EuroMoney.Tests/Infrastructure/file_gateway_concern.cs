﻿using System;
using System.Collections.Generic;
using System.IO;
using EuroMoney.Infrastructure.FileGateway;
using EuroMoney.Infrastructure.FileGateway.Contract;
using Machine.Specifications;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace EuroMoney.Tests.Infrastructure
{
    public abstract class file_gateway_concern : Observes<IFileGateway, FileGateway>
    {
        protected static IFileGateway fileGateway;
        protected static MemoryStream result;
        protected static string fileName;
        protected Establish context = () => { fileName = AppDomain.CurrentDomain.BaseDirectory + "../../../person-3.txt"; };
    }

    public class when_asked_to_open_a_file : file_gateway_concern
    {
        private Because b = () => { result = sut.GetContentsFromFile(fileName); };
        private It should_open_the_file = () => result.ShouldNotBeNull();
    }

    public class when_asked_to_open_a_non_existent_file : file_gateway_concern
    {
        private Establish context = () =>
            {
                fileName = "FILE_DOESNT_EXIST";
            };

        private Because b = () => spec.catch_exception(() => sut.GetContentsFromFile(fileName));

        private It should_return_file_doesnt_exist_exception = () => spec.exception_thrown.ShouldBeAn<FileNotFoundException>();
    }

    public class when_asked_to_save_dictionary_to_file : file_gateway_concern
    {
        private Establish c = () =>
            {
		personDictionary = new Dictionary<string, string> {{"id", "4"}, {"familyName", "test"}};
            };

        private Because b = () => sut.Save(personDictionary);
        private static IDictionary<string, string> personDictionary;

        private It should_create_new_file = () => { };
    }
}