﻿using System;
using System.IO;
using System.Text;
using EuroMoney.Domain;
using EuroMoney.Infrastructure.FileGateway.Contract;
using EuroMoney.Infrastructure.PersonGateway;
using EuroMoney.Infrastructure.PersonGateway.Contracts;
using Machine.Specifications;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace EuroMoney.Tests.Infrastructure 
{
    public abstract class filestream_to_domain_concern : Observes<IPersonGateway, PersonGateway>
    {
        protected static Person result;
        protected static MemoryStream memoryStream;
        protected const string TEST_STRING = "id=3";
        protected static IFileGateway fileGateway;
        protected static string fileName;
    }

    public class when_person_mapper_is_asked_to_map_from_filestream : filestream_to_domain_concern
    {
        private Establish context = () =>
            {
                memoryStream = new MemoryStream(Encoding.Default.GetBytes(TEST_STRING));
                fileGateway = depends.on<IFileGateway>();
                id = 3;
                var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                fileName = string.Format("{0}\\person-{1}.txt", baseDirectory, id.ToString());               
                fileGateway.setup(x => x.GetContentsFromFile(fileName)).Return(memoryStream);
            };

        private Because b = () => { result = sut.Serialize(id); };

        private It should_ask_filegateway_to_return_filestream =
            () => fileGateway.received(x => x.GetContentsFromFile(fileName));

        private It should_return_person_object = () => result.ShouldNotBeNull();

        private It should_return_person_id = () => result.Id.ShouldEqual(3);
        private static int id;
    }
}
