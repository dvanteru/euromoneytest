﻿using EuroMoney.Domain;
using EuroMoney.Infrastructure.FileGateway.Contract;
using EuroMoney.Infrastructure.PersonGateway.Contracts;
using EuroMoney.Infrastructure.Repository.PersonRepository;
using EuroMoney.Infrastructure.Repository.PersonRepository.Contracts;
using Machine.Specifications;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace EuroMoney.Tests.Infrastructure 
{
    public abstract class person_repository_concern : Observes<IPersonRepository, PersonRepository>
    {
        protected static Person result;
    }

    public class when_asked_to_get_person_by_id : person_repository_concern
    {
        private Establish context = () =>
            {
		result = new Person{Id = 3};
                personGateway = depends.on<IPersonGateway>();
                personGateway.setup(x => x.Serialize(id)).Return(result);
            };

        private static int id = 3;
        private Because b = () => result = sut.Get(id);

        private It should_ask_person_mapper_to_get_person = () => personGateway.received(x => x.Serialize(id));

        private It should_return_person_entity = () => result.ShouldNotBeNull();
        private static IPersonGateway personGateway;
    }

    public class when_asked_to_save_person : person_repository_concern
    {
        private Establish c = () =>
            {
		person = new Person() {Id = 4, FamilyName = "test"};
                personGateway = depends.on<IPersonGateway>();
            };

        private Because b = () => sut.Save(person);

        private It should_ask_person_gateway_save_person = () => personGateway.received(x => x.Save(person));
        private static Person person;
        private static IPersonGateway personGateway;
    }
}
