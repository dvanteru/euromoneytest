﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.WebApi;
using EuroMoney.Controllers.Person;
using EuroMoney.Controllers.Person.ViewModels.Mapper;
using EuroMoney.Controllers.Person.ViewModels.Mapper.Contract;
using EuroMoney.Infrastructure.FileGateway;
using EuroMoney.Infrastructure.FileGateway.Contract;
using EuroMoney.Infrastructure.PersonGateway;
using EuroMoney.Infrastructure.PersonGateway.Contracts;
using EuroMoney.Infrastructure.Repository.PersonRepository;
using EuroMoney.Infrastructure.Repository.PersonRepository.Contracts;

namespace EuroMoney 
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication 
    {
        protected void Application_Start() 
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetAssembly(typeof(PersonController)));
            builder.RegisterType<PersonRepository>().As<IPersonRepository>().InstancePerApiRequest();
            builder.RegisterType<PersonGateway>().As<IPersonGateway>();
            builder.RegisterType<FileGateway>().As<IFileGateway>();
            builder.RegisterType<PersonPageViewModelMapper>().As<IPersonPageViewModelMapper>();
            builder.RegisterType<PersonModelMapper>().As<IPersonModelMapper>();

            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;

            AreaRegistration.RegisterAllAreas();
	    WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
	    

        }
    }
}